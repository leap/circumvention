# circumvention

a place to discuss & coordinate censorship-circumvention research


## 永恆的戰爭

"there is an eternal war between the firewall and VPNs" -- [Binxing Fang](https://en.wikipedia.org/wiki/Fang_Binxing).

"desire is irrelevant. I am a machine" -- John Connor.


## What is this place?


![confused gandalf](./6jn48q.jpg)

This repo intends to act as a one-stop for planning, research, and commenting
on relevant technologies. No code should live here, it's just a living
documentation place. Feel free to use the issues and link to ongoing issues in
other code projects.

## Useful pointers

* [Obfuscating OpenVPN with ObfsVPN: A tutorial in two parts](https://docs.leap.se/tutorials/obfsvpn-part-1/).
* The [invisible library](https://github.com/leapcode/library).
* [obfsvpn](https://0xacab.org/leap/obfsvpn)
* consolidated [documentation](https://0xacab.org/leap/dev-documentation) of the LEAP VPN API and [architectural overview](https://docs.leap.se/overview/).
* [menshen](https://0xacab.org/leap/menshen) the guardian of gates.

## Nearby

* https://github.com/net4people/bbs
* https://gitlab.torproject.org/tpo/anti-censorship
* https://www.pluggabletransports.info/
* https://www.v2ray.com/en/
* https://github.com/HyNetwork/hysteria
